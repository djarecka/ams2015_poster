* Testing and Python

  - tests rises your confidence that your code works properly
  
  - tests allows you to make faster and reliable changes      

  - Python has offers variety of testing frameworks: unittest, nose, pytest 


* Pytest - useful features:
  
  - no-boilerplate test code - easy to learn 

  def test_your_function(x):
      assert (your_function(x) > 0)

  - parametrization of test function - wide range of parameters can be change at the same time

  @pytest.mark.parametrize("input,expected", [
    (-2, 4),
    ( 2, 4),
       ])  
   def test_your_function(input, expected):
        assert your_function(input) == expected

  - skip and xfail - easy dealing with test that can currently fail

  @pytest.mark.parametrize("input,expected", [
    (-2, 4),
     pytest.mark.xfail(reason="some bug for negative values")( 2, 4),
       ])
   def test_your_function(input, expected):
        assert your_function(input) == expected


   -  pytest_generate_tests - generating set of tests depending on command lines (e.q. allows for testing various library at the same time)

   # content of conftest.py 

   def pytest_addoption(parser):
       parser.addoption("--libname", action="append", default=[], 
                         help="tested library")

   def pytest_generate_tests(metafunc):
       if 'libname' in metafunc.fixturenames:
       	    metafunc.parametrize("libname", metafunc.config.option.libname)

   # content of test_yourcode.py
   def test_your_function(libname, input, expected):
       assert function(libname, input) == expected

   
   # the test can be run:
   $ python2.7 -m pytest -s blk_1m_pytest.py --libname=wrfkessler_blk_1m_pytest

   
  - (?) integrates with other testing methods and tools (can run nose, unittest and doctest style test suites)  


